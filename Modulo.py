import requests, json, scripts, os
from time import sleep

flag_menu = True
lista_de_filmes = []

while flag_menu:

    scripts.menu()

    escolha = str(input('Digite o que deseja: '))

    if escolha == '1':

        scripts.adicionar_filme()
        
    elif escolha == '2':

        scripts.deletar_filme()
    
    elif escolha == '3':

       scripts.visualizar_lista_de_desejos()

    elif escolha == '4':
        
        scripts.buscar_filme()          

    elif escolha == '5':
        
        scripts.status()

    elif escolha == '6':
        
        scripts.sugestoes_por_genero()   

    elif escolha == '7':

        scripts.sugestoes_pelo_ultimo_filme()

    elif escolha == '8':

        scripts.sugestoes_por_todos_os_filmes()

    elif escolha == '9':

        print('\nAté um outro dia...... Um triste amanhã...... :)\n')
        flag_menu = False     

    else:
        print('\nOpção inválida, por favor, escolha uma opção válida!.')