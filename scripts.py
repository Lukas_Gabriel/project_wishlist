import requests, json, time, os
from time import sleep

def menu():

    print('\n****** Bem vindo(a) a sua lista de desejos! ******\n')
    print('*** Adicionar filme a lista de desejos: [1] ***')
    print('*** Deletar filme da lista de desejos: [2] ***')
    print('*** Visualizar a lista de desejos: [3] ***')
    print('*** Buscar filme na lista de desejos: [4] ***')
    print('*** Atualizar status de filme da lista de desejos: [5] ***')
    print('*** Sugestões com base em um gênero: [6] ***')
    print('*** Sugestões com base no último filme adicionado: [7] ***')
    print('*** Sugestões com base em todos os filmes adicionados: [8] ***')
    print('*** Sair da lista de desejos: [9] ***')
    print('\n***********************************************\n')

def status():

    flag_status = False
        
    try:    
        
        with open('lista_de_desejos.json') as arquivo:
            reference = json.load(arquivo)
            
            if len(reference) != 0:
                
                print('\nEsses são os filmes que você possui na sua lista de desejos.\n')

                for filme_na_lista in reference:
            
                    print(f'\nO título do filme é: {filme_na_lista["Titulo"]}')
                    print(f'O ano do filme é: {filme_na_lista["Ano"]}')
                    print(f'O diretor do filme é: {filme_na_lista["Diretor"]}')
                    print(f'Os atores principais são: {filme_na_lista["Atores Principais"]}')
                    print(f'O tempo de duração do filme é: {filme_na_lista["Tempo De Tela"]}')
                    print(f'O(s) gênero(s) do filme é(são): {filme_na_lista["Genero"]}')
                    print(f'O país de origem do filme é: {filme_na_lista["Nacionalidade"]}')
                    print(f'A lingua original do filme é: {filme_na_lista["Lingua Original"]}')
                    print(f'Status De Visualização: {filme_na_lista["Status De Visualizacao"]}')
            
        title_status = str(input('\nDigite o nome do filme que deseja alterar o status de visualização: ')).lower().strip()
        
        with open('lista_de_desejos.json') as arquivo:
            
            reference = json.load(arquivo)

            for filme_na_lista in reference:
                
                if title_status == filme_na_lista['Titulo']:
                    
                    flag_status = True  
            
            if flag_status:     
                print('\nOpções De Status De Visualização: ')
                print('\nAssistido [1]')
                print('Não assistido [2]')
                print('Assistir mais tarde [3]\n')
                    
                escolha_de_status = input('Digite o número da escolha de status desejada: ')

                if escolha_de_status == '1':

                    filme_na_lista['Status De Visualizacao'] = 'Assistido'

                    with open('lista_de_desejos.json', 'w') as arquivo:    
                        json.dump(reference, arquivo, indent=4)
                        print('\nStatus atualizado!')
                        voltando_ao_menu()

                elif escolha_de_status == '2':

                    filme_na_lista['Status De Visualizacao'] = 'Nao assistido'
                        
                    with open('lista_de_desejos.json', 'w') as arquivo:    
                        json.dump(reference, arquivo, indent=4)
                        print('\nStatus atualizado!')
                        voltando_ao_menu()

                elif escolha_de_status == '3':

                    filme_na_lista['Status De Visualizacao'] = 'Assistir mais tarde' 
                        
                    with open('lista_de_desejos.json', 'w') as arquivo:    
                        json.dump(reference, arquivo, indent=4)
                        print('\nStatus atualizado!')
                        voltando_ao_menu()

                        
                else:
                        
                        print('\nOpção inválida.')
                        voltando_ao_menu()
                
            else:
                pass               
            
            if flag_status == False:
                    
                print('\nVocê não possui esse filme na sua lista')
                voltando_ao_menu()

            else:
                pass    

    except FileNotFoundError:
        
        print('\nVocê ainda não possui uma lista de desejos. ):')  
        voltando_ao_menu()    

    except KeyError:

        print('Esse não é um filme válido.')

def voltando_ao_menu():  
    
    print('\nRetornando ao menu...')
    sleep(2)          

def visualizar_lista_de_desejos():

    try:
            
        flag = True

        with open('lista_de_desejos.json') as arquivo_tres:
            reference_tres = json.load(arquivo_tres)
                      
            if len(reference_tres) == 0:
                print('\nVocê não tem nenhum filme na sua lista de desejos!')
                flag = False
                           
            if flag:

                    print(f'\nVocê possui {len(reference_tres)} filme(s) na sua lista de desejos.')
                    print('\nO(s) filme(s) na sua lista é(são):')
                    
                    for filme_na_lista in reference_tres:

                        print(f'\nO título do filme é: {filme_na_lista["Titulo"]}')
                        print(f'O ano do filme é: {filme_na_lista["Ano"]}')
                        print(f'O diretor do filme é: {filme_na_lista["Diretor"]}')
                        print(f'Os atores principais são: {filme_na_lista["Atores Principais"]}')
                        print(f'O tempo de duração do filme é: {filme_na_lista["Tempo De Tela"]}')
                        print(f'O(s) gênero(s) do filme é(são): {filme_na_lista["Genero"]}')
                        print(f'O país de origem do filme é: {filme_na_lista["Nacionalidade"]}')
                        print(f'A lingua original do filme é: {filme_na_lista["Lingua Original"]}')
                        print(f'Status De Visualização: {filme_na_lista["Status De Visualizacao"]}')
                            
            else:
                pass

            voltando_ao_menu()

    except FileNotFoundError:
        
        print('\nNenhuma lista de desejos encontrada. ):')
        voltando_ao_menu()
        
def buscar_filme():

    filme_desejado = str(input('\nDigite o nome do filme que deseja buscar na sua lista de desejos: ')).strip()
        
    flag_quatro = True
        
    try:
        with open('lista_de_desejos.json') as arquivo_tres:
            reference_quatro = json.load(arquivo_tres)

        for filme_na_lista in reference_quatro:
               
            if filme_desejado == filme_na_lista['Titulo']:
                
                flag_quatro = False

                print(f'\nO título do filme é: {filme_na_lista["Titulo"]}')
                print(f'O ano do filme é: {filme_na_lista["Ano"]}')
                print(f'O diretor do filme é: {filme_na_lista["Diretor"]}')
                print(f'Os atores principais são: {filme_na_lista["Atores Principais"]}')
                print(f'O tempo de duração do filme é: {filme_na_lista["Tempo De Tela"]}')
                print(f'O(s) gênero(s) do filme é(são): {filme_na_lista["Genero"]}')
                print(f'O país de origem do filme é: {filme_na_lista["Nacionalidade"]}')
                print(f'A lingua original do filme é: {filme_na_lista["Lingua Original"]}')
                print(f'Status De Visualização: {filme_na_lista["Status De Visualizacao"]}')

                voltando_ao_menu()

            else:
                
                pass    
            
        if flag_quatro == True:
            
            print('\nVocê não possui este filme na sua lista de desejos!')
            voltando_ao_menu()

        else:
            pass  

    except FileNotFoundError:
            
        print('\nNenhuma lista de desejos encontrada. ):')
        voltando_ao_menu() 

def deletar_filme():

    try:
        
        with open('lista_de_desejos.json') as arquivo:
            reference = json.load(arquivo)
            
            if len(reference) != 0:
                
                print('\nEsses são os filmes que você possui na sua lista de desejos.')
                
                for filme_na_lista in reference:
                    
                    print(f'\nO título do filme é: {filme_na_lista["Titulo"]}')
                    print(f'O ano do filme é: {filme_na_lista["Ano"]}')
                    print(f'O diretor do filme é: {filme_na_lista["Diretor"]}')
                    print(f'Os atores principais são: {filme_na_lista["Atores Principais"]}')
                    print(f'O tempo de duração do filme é: {filme_na_lista["Tempo De Tela"]}')
                    print(f'O(s) gênero(s) do filme é(são): {filme_na_lista["Genero"]}')
                    print(f'O país de origem do filme é: {filme_na_lista["Nacionalidade"]}')
                    print(f'A lingua original do filme é: {filme_na_lista["Lingua Original"]}')
                    print(f'Status De Visualização: {filme_na_lista["Status De Visualizacao"]}')

            else:

                print('Você não possui nenhum filme na sua lista de desejos, voltando ao menu, adicione filme a lista!')        

        title_dois = str(input('\nDigite o nome do filme que deseja deletar na sua lista de desejos: ')).lower().strip()
            
        with open('lista_de_desejos.json') as arquivo_dois: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
            reference_dois = json.load(arquivo_dois) 

        flag_tres = True
                
        for movie2 in reference_dois:
                
            if title_dois == movie2["Titulo"]:# Fazendo a comparação entre o filme que foi digitado e os filmes já salvos na lista de desenjos
                            
                flag_tres = False

            else:
                pass       
                
        if flag_tres == False:
                    
            lista = []

            for filme_delete in reference_dois:

                if title_dois != filme_delete["Titulo"]:
                            
                    lista.append(filme_delete)


            with open('lista_de_desejos.json', 'w') as arquivo_dois:
                json.dump(lista, arquivo_dois, indent=4)
                    

            print('\nFilme apagado da lista de desejos com sucesso!')
            voltando_ao_menu()
                
        elif flag_tres:
            print('\nEsse filme não existe na sua lista de desejos!')
            voltando_ao_menu()
        
    except FileNotFoundError:
        print('\nLista de desejos não existe.')  
        voltando_ao_menu()
    
    except KeyError:
        print('\nNão é um filme válido.')
        voltando_ao_menu()  

def sugestoes_com_base_em_todos_os_filmes_adicionados():

    try: 

        with open ('lista_de_desejos.json') as arquivo:
            
            reference = json.load(arquivo)

            if len(reference) != 0:

                cont = 1

                print("\nVocê pode adicionar as sugestões a sua lista de desejos em 'adicionar filme fornecendo o título e o ano do filme', \napós escolher adicionar filme a lista de desejos no menu.\n")
                print('Temos algumas sugestões com base no seu último filme adicionado!\n ')

                for filme in reference:

                    nome = filme["Titulo"]

                    consulta = requests.get('http://www.omdbapi.com/?apikey=e85dca2c&s={}'.format(nome)).json()
                   
                    if 'Error' not in consulta and 'False' not in consulta:    
                        
                        sugestoes = consulta["Search"]
                        
                        for filme in sugestoes:

                            if filme["Type"] == "movie":
                            
                                print(f'Sugestão {cont}:')
                                print(f'\nO título do filme é: {filme["Title"]}')
                                print(f'O ano do filme é: {filme["Year"]}\n')
                            
                                cont += 1

                            else:
                                pass               
                    
                    else: 
                        pass    

            else:
                print('Você não possui nenhum filme na sua lista!')
                voltando_ao_menu()            
        
        voltando_ao_menu()

    except FileNotFoundError:
        
        print('\nNão é possível sugerir mídias, pois você ainda não possui uma lista de desejos, de volta ao menu, vá em adicionar filme e crie uma!')
        voltando_ao_menu()   

def adicionar_filme():

    lista_de_filmes = []
   
    try:
             
        title = str(input('\nDigite o nome do filme que deseja buscar: ')).lower().strip()
        
        consulta = requests.get(f'http://www.omdbapi.com/?apikey=e85dca2c&s={title}').json()# Fazendo a chamada da API com base no título do filme

        retornos_de_filmes_da_consulta = consulta["Search"]                  
        
        cont = 1
        
        print(f'\nEncontramos alguns filmes relacionados a {title.title()}, são eles:\n')
        
        for filme in retornos_de_filmes_da_consulta:

           if filme["Type"] == 'movie':
           
                print(f'Filme de número: {cont}')
                print(f'\nO título do filme é: {filme["Title"]}')
                print(f'O ano do filme é: {filme["Year"]}\n')
                cont += 1
            
        nome_do_filme = str(input('Digite o nome do filme que deseja adicionar a sua lista de desejos: '))
        ano_do_filme = int(input('Digite o ano do filme que deseja adicionar a sua lista de desejos: '))

        consulta = requests.get('http://www.omdbapi.com/?apikey=e85dca2c&t={}&y={}'.format(nome_do_filme, ano_do_filme)).json()

        print(f'\nO título do filme é: {consulta["Title"]}')
        print(f'O ano do filme é: {consulta["Year"]}')
        print(f'O diretor do filme é: {consulta["Director"]}')
        print(f'Os atores principais são: {consulta["Actors"]}')
        print(f'O tempo de duração do filme é: {consulta["Runtime"]}')
        print(f'O(s) gênero(s) do filme é(são): {consulta["Genre"]}')
        print(f'O país de origem do filme é: {consulta["Country"]}')
        print(f'A lingua original do filme é: {consulta["Language"]}')

        esc1 = str(input('\nDeseja adicionar este filme a sua lista de desejos [S/N]: ')).upper().strip()# Retornando a str maiúscula sem espaços no começo e no final    

        if esc1 == 'S':

            filme = { # Salvando em um dicionário as informações que me interessam do filme, obtidas através da API 
                "Titulo": consulta['Title'].lower(),
                "Ano": consulta['Year'],
                "Diretor": consulta['Director'],
                "Atores Principais": consulta['Actors'],
                "Tempo De Tela": consulta['Runtime'],
                "Genero": consulta['Genre'],
                "Nacionalidade": consulta['Country'],
                "Lingua Original": consulta['Language'],
                "Status De Visualizacao": 'A definir'            
                    }
            
            try:
              
                with open('lista_de_desejos.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                    reference = []
                    reference = json.load(arquivo)

                flag_dois = True

                for movie in reference:
                    
                    if filme["Titulo"] == movie['Titulo']:
                        
                        print('\nEsse filme já existe na sua lista!')
                        voltando_ao_menu()
                        flag_dois = False
                
                    else:
                        pass
                        
                if flag_dois == True:

                    reference.append(filme) #Apendando no meu arquivo de filmes o novo filme
                        
                    with open('lista_de_desejos.json', 'w') as arquivo: #Abrindo meu arquivo de filmes como escrita
                        
                        json.dump(reference, arquivo, indent=4) # Salvando as alterações feitas ao adicionar o novo filme
                        print('\nFilme adicionado com sucesso!') 

                    voltando_ao_menu()
                
                else:
                    pass        

            except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
                lista_de_filmes.append(filme)
                    
                print('\nNenhuma lista de desejos encontrada.... Criando lista de desejos e adicionando filme....')
                    
                with open('lista_de_desejos.json', 'w') as arquivo:
                    json.dump(lista_de_filmes, arquivo, indent=4)
                    
                sleep(2)
                    
                print('\nLista criada e filme adicionado!')
                voltando_ao_menu()

        elif esc1 == 'N':

            voltando_ao_menu()
        
        else:
            print('\nOpção inválida.')
            voltando_ao_menu()

    except ConnectionError:
                
        print('Erro de conexão!')
        voltando_ao_menu()

    except ValueError:
                
        print('\nNão foi digitado o ano e/ou o nome de um filme válido!')
        voltando_ao_menu()

    except UnboundLocalError:
        
        print('Não foi possivel realizar a ação.')
        voltando_ao_menu()                   

    except KeyError:

        consulta = requests.get('http://www.omdbapi.com/?apikey=e85dca2c&t={}'.format(title)).json()
        print(f'\nO título do filme é: {consulta["Title"]}')
        print(f'O ano do filme é: {consulta["Year"]}')
        print(f'O diretor do filme é: {consulta["Director"]}')
        print(f'Os atores principais são: {consulta["Actors"]}')
        print(f'O tempo de duração do filme é: {consulta["Runtime"]}')
        print(f'O(s) gênero(s) do filme é(são): {consulta["Genre"]}')
        print(f'O país de origem do filme é: {consulta["Country"]}')
        print(f'A lingua original do filme é: {consulta["Language"]}')   

        esc1 = str(input('\nDeseja adicionar este filme a sua lista de desejos [S/N]: ')).upper().strip()# Retornando a str maiúscula sem espaços no começo e no final    

        if esc1 == 'S':

            filme = { # Salvando em um dicionário as informações, obtidas através da API, que me interessam do filme 
                "Titulo": consulta['Title'].lower(),
                "Ano": consulta['Year'],
                "Diretor": consulta['Director'],
                "Atores Principais": consulta['Actors'],
                "Tempo De Tela": consulta['Runtime'],
                "Genero": consulta['Genre'],
                "Nacionalidade": consulta['Country'],
                "Lingua Original": consulta['Language'],
                "Status De Visualizacao": 'A definir'            
                    }
            
            try:
              
                with open('lista_de_desejos.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                    reference = []
                    reference = json.load(arquivo)

                flag_dois = True

                for movie in reference:
                    
                    if filme["Titulo"] == movie['Titulo']:
                        
                        print('\nEsse filme já existe na sua lista!')
                        voltando_ao_menu()
                        flag_dois = False
                
                    else:
                        pass
                        
                if flag_dois == True:

                    reference.append(filme) #Apendando no meu arquivo de filmes o novo filme
                        
                    with open('lista_de_desejos.json', 'w') as arquivo: #Abrindo meu arquivo de filmes como escrita
                        
                        json.dump(reference, arquivo, indent=4) # Salvando as alterações feitas ao adicionar o novo filme
                        print('\nFilme adicionado com sucesso!') 

                    voltando_ao_menu()
                
                else:
                    pass        

            except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
                lista_de_filmes.append(filme)
                    
                print('\nNenhuma lista de desejos encontrada.... Criando lista de desejos e adicionando filme....')
                    
                with open('lista_de_desejos.json', 'w') as arquivo:
                    json.dump(lista_de_filmes, arquivo, indent=4)
                    
                sleep(2)
                    
                print('\nLista criada e filme adicionado!')
                voltando_ao_menu()

        elif esc1 == 'N':

            voltando_ao_menu()
        
        else:
            print('\nOpção inválida.')
            voltando_ao_menu() 

def adicionar_filme_no_repositorio():

    filmes_repositorio = []
    
    titulo = str(input('\nDigite o nome do filme que deseja buscar: ')).lower().strip()
    ano = int(input('Digite o ano do filme que deseja buscar: '))

    try:
    
        consulta = requests.get(f'http://www.omdbapi.com/?apikey=e85dca2c&t={titulo}&y={ano}').json()

        print(f'\nO título do filme é: {consulta["Title"]}')
        print(f'O ano do filme é: {consulta["Year"]}')
        print(f'O diretor do filme é: {consulta["Director"]}')
        print(f'Os atores principais são: {consulta["Actors"]}')
        print(f'O tempo de duração do filme é: {consulta["Runtime"]}')
        print(f'O(s) gênero(s) do filme é(são): {consulta["Genre"]}')
        print(f'O país de origem do filme é: {consulta["Country"]}')
        print(f'A lingua original do filme é: {consulta["Language"]}')

        escolha_repositorio = str(input('\nDeseja adicionar este filme a sua lista de desejos [S/N]: ')).upper().strip()

        if escolha_repositorio == 'S':

            filme = { # Salvando em um dicionário as informações, obtidas através da API, que me interessam do filme 
                
                "Titulo": consulta['Title'].lower(),
                "Ano": consulta['Year'],
                "Diretor": consulta['Director'],
                "Atores Principais": consulta['Actors'],
                "Tempo De Tela": consulta['Runtime'],
                "Genero": consulta['Genre'],
                "Nacionalidade": consulta['Country'],
                "Lingua Original": consulta['Language'],            
                }
            
            try:
              
                with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                    reference = []
                    reference = json.load(arquivo)

                flag = True

                for movie in reference:
                    
                    if filme["Titulo"] == movie['Titulo']:
                        
                        print('\nEsse filme já existe no seu repositório!')
                        voltando_ao_menu()
                        flag = False
                
                    else:
                        pass
                        
                if flag == True:

                    reference.append(filme) #Apendando no meu arquivo de filmes o novo filme
                        
                    with open('repositorio_de_filmes.json', 'w') as arquivo: #Abrindo meu arquivo de filmes como escrita
                        
                        json.dump(reference, arquivo, indent=4) # Salvando as alterações feitas ao adicionar o novo filme
                        print('\nFilme adicionado com sucesso ao repositório!') 

                    voltando_ao_menu()
                
                else:
                    pass        

            except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
                filmes_repositorio.append(filme)
                    
                print('\nNenhum repositório encontrado.... Criando repositório de filmes e adicionando filme....')
                    
                with open('repositorio_de_filmes.json', 'w') as arquivo:
                    json.dump(filmes_repositorio, arquivo, indent=4)
                    
                sleep(2)
                    
                print('\nRepositório criado e filme adicionado!')
                voltando_ao_menu()

        elif escolha_repositorio == 'N':

            voltando_ao_menu()

        else:

            print('\nOpção inválida!')    
    
    except KeyError:

        print('\nNome do filme ou ano incorreto!')
        voltando_ao_menu()    

def deletar_filme_no_repositorio():

    try:
        
        with open('repositorio_de_filmes.json') as arquivo:
            
            reference = json.load(arquivo)
            
            if len(reference) != 0:
                
                print('\nEsses são os filmes que você possui no seu repositório.')
                
                for filme_no_repositorio in reference:
                    
                    print(f'\nO título do filme é: {filme_no_repositorio["Titulo"]}')
                    print(f'O ano do filme é: {filme_no_repositorio["Ano"]}')
                    print(f'O diretor do filme é: {filme_no_repositorio["Diretor"]}')
                    print(f'Os atores principais são: {filme_no_repositorio["Atores Principais"]}')
                    print(f'O tempo de duração do filme é: {filme_no_repositorio["Tempo De Tela"]}')
                    print(f'O(s) gênero(s) do filme é(são): {filme_no_repositorio["Genero"]}')
                    print(f'O país de origem do filme é: {filme_no_repositorio["Nacionalidade"]}')
                    print(f'A lingua original do filme é: {filme_no_repositorio["Lingua Original"]}')

            

                titulo = str(input('\nDigite o nome do filme que deseja deletar do seu repositório de filmes: ')).lower().strip()
            
                with open('repositorio_de_filmes.json') as arquivo_dois: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                    
                    reference = json.load(arquivo_dois) 

                    flag = True
                
                    for movie in reference:
                
                        if titulo == movie["Titulo"]: # Fazendo a comparação entre o filme que foi digitado e os filmes já salvos na lista de desenjos
                                
                            flag = False

                    else:
                        pass       
                    
                    if flag == False:
                        
                        lista = []

                        for filme_delete in reference:

                            if titulo != filme_delete["Titulo"]:
                                
                                lista.append(filme_delete)


                        with open('repositorio_de_filmes.json', 'w') as arquivo_dois:
                            json.dump(lista, arquivo_dois, indent=4)
                        

                        print('\nFilme apagado do repositório com sucesso!')
                        voltando_ao_menu()
                    
                    elif flag:
                
                        print('\nEsse filme não existe no seu repositório!')
                        voltando_ao_menu()

            else:

                print('\nVocê não possui nenhum filme no seu repositório!') 
                voltando_ao_menu()
        
    except FileNotFoundError:
        
        print('\nRepositório não existe.')  
        voltando_ao_menu()
    
    except KeyError:
        
        print('\nNão é um filme válido.')
        voltando_ao_menu()

def menu_repositorio():

    print('\n*****************************************')
    print('\nBem vindo ao seu repositório de filmes!')
    print('\nAdicionar filmes ao repositório: [1]')
    print('Apagar filme do repositório: [2]')
    print('Sair do repositório: [3]')
    print('\n*****************************************')

def sugestoes_por_genero():

    print('\nDeseja sugestões com base em que gênero: \n')
    print('Adventure: [1]')
    print('Action: [2]')
    print('Comedy: [3]')
    print('Horror: [4]')
    print('Thriller: [5]')
    print('Mystery: [6]')
    print('Family: [7]')
    print('Sci-Fi: [8]')
    print('Crime: [9]')
    print('Animation: [10]')
    print('Drama: [11]')
    print('Romance: [12]\n')

    escolha_de_genero = str(input('Digite a opção do gênero que deseja: '))


    if escolha_de_genero == '1':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Adventure: ')
                    print('\n-----------------------------------------------\n')

                    cont = 1

                    for filme in reference:
                    
                        if 'Adventure' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Adventure')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        
                    print('\n-----------------------------------------------\n')

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')

    elif escolha_de_genero  == '2':
    
        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Action' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Action')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '3':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Comedy' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Comedy')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '4':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Horror' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Horror')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '5':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Thriller' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Thriller')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '6':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Mystery' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Mystery')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '7':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Family' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Family')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '8':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Sci-Fi' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Sci-Fi')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '9':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Crime' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Crime')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero  == '10':
                                                
        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Animation' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Animation')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()

    elif escolha_de_genero == '11':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Drama' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Drama')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    elif escolha_de_genero == '12':

        try:
              
            with open('repositorio_de_filmes.json') as arquivo: #Abrindo e carregando o arquivo de filmes para uma variável refêrencia
                reference = []
                reference = json.load(arquivo)

                if len(reference) != 0:
                
                    print('\nAlgumas sugestões de filmes relacionados com o gênero Horror: ')
                    
                    cont = 1

                    for filme in reference:
                    
                        if 'Romance' in  filme["Genero"] :
                        
                            print(f'\nSugestão {cont}:')
                            print(f'\nO título do filme é: {filme["Titulo"]}')
                            print(f'O ano do filme é: {filme["Ano"]}')
                            print(f'O diretor do filme é: {filme["Diretor"]}')
                            print(f'Os atores principais são: {filme["Atores Principais"]}')
                            print(f'O tempo de duração do filme é: {filme["Tempo De Tela"]}')
                            print(f'O(s) gênero(s) do filme é(são): Romance')
                            print(f'O país de origem do filme é: {filme["Nacionalidade"]}')
                            print(f'A lingua original do filme é: {filme["Lingua Original"]}')

                            cont += 1
                
                        else:
                            pass

                    print('\nVocê pode adicionar qualquer um desses filmes a sua lista de desejos,\n após voltar ao menu, selecione a opção de adicionar filme, informe o nome do filme que deseja adicionar,\n após os resultados da busca serem mostrados, digite o nome e o ano do filme.')        

                else:

                    print('Você não possui nenhum filme no seu repositório.')
                    voltando_ao_menu()          

        except FileNotFoundError: #Caso não encontre o arquivo .json cria e adiciona o filme escolhido
                    
            print('Não foi encontrado nenhum repositório de filmes, voltando ao menu, adicione um filme e seu repositório será criado!')
            voltando_ao_menu()
    
    else:

        print('Escolha inválida, tente novamente.')
        voltando_ao_menu()

def sugestoes_pelo_ultimo_filme():

    try:
        
        contador = 1

        with open ('lista_de_desejos.json') as arquivo:

            reference = json.load(arquivo)

            if len(reference) != 0:

                print('\nTemos algumas sugestões com base no seu último filme adicionado!\n ')
                
                for filme in reference:

                    if filme == reference[len(reference) - 1]:

                        genero_do_ultimo_filme = filme['Genero']
                        gen = genero_do_ultimo_filme.split()

                        for item in gen:

                            with open('repositorio_de_filmes.json') as arquivo2:
                                rep = json.load(arquivo2)

                                for filme_rep in rep:

                                    if item in filme_rep['Genero']:

                                        print(f'\nSugestão {contador}:')
                                        print(f'\nO título do filme é: {filme_rep["Titulo"]}')
                                        print(f'O ano do filme é: {filme_rep["Ano"]}')
                                        print(f'O diretor do filme é: {filme_rep["Diretor"]}')
                                        print(f'Os atores principais são: {filme_rep["Atores Principais"]}')
                                        print(f'O tempo de duração do filme é: {filme_rep["Tempo De Tela"]}')
                                        print(f'O(s) gênero(s) do filme é(são): {filme_rep["Genero"]}')
                                        print(f'O país de origem do filme é: {filme_rep["Nacionalidade"]}')
                                        print(f'A lingua original do filme é: {filme_rep["Lingua Original"]}')

                                        contador += 1  

                        print("\nVocê pode adicionar as sugestões a sua lista de desejos fornecendo o título e o ano do filme, \napós escolher a opção 1 no menu, 'adicionar filme a lista de desejos'.")                          

                    else:
                        
                        pass
            
            else:
                print('\nNão é possível sugerir mídias, pois você ainda não possui nenhum filme adicionado a sua lista para ser usado como base.')
                voltando_ao_menu()


    except FileNotFoundError:
        print("\nNão é possível sugerir mídias, pois você ainda não possui uma lista de desejos, de volta ao menu, vá em 'adicionar filme a lista de desejos' e crie uma!")
        voltando_ao_menu()

def sugestoes_por_todos_os_filmes():

    try:
        
        contador = 1
 
        with open ('lista_de_desejos.json') as arquivo:

            reference = json.load(arquivo)

            if len(reference) != 0:
                
                print('\nTemos algumas sugestões com base no seu último filme adicionado!\n ')
                
                for filme in reference:

                    genero_do_ultimo_filme = filme['Genero']
                    gen = genero_do_ultimo_filme.split()

                    for item in gen:

                        with open('repositorio_de_filmes.json') as arquivo2:
                        
                            rep = json.load(arquivo2)

                            for filme_rep in rep:

                                if item in filme_rep['Genero']:

                                    print(f'\nSugestão {contador}:')
                                    print(f'\nO título do filme é: {filme_rep["Titulo"]}')
                                    print(f'O ano do filme é: {filme_rep["Ano"]}')
                                    print(f'O diretor do filme é: {filme_rep["Diretor"]}')
                                    print(f'Os atores principais são: {filme_rep["Atores Principais"]}')
                                    print(f'O tempo de duração do filme é: {filme_rep["Tempo De Tela"]}')
                                    print(f'O(s) gênero(s) do filme é(são): {filme_rep["Genero"]}')
                                    print(f'O país de origem do filme é: {filme_rep["Nacionalidade"]}')
                                    print(f'A lingua original do filme é: {filme_rep["Lingua Original"]}')
                                    print('--------------------------------------------------')
                                    print('=================================================================')
                                    print('================================================================================')

                                    contador += 1  

                print("\nVocê pode adicionar as sugestões a sua lista de desejos fornecendo o título e o ano do filme, \napós escolher a opção 1 no menu, 'adicionar filme a lista de desejos'.")                          
            
            else:
                
                print('\nNão é possível sugerir mídias, pois você ainda não possui nenhum filme adicionado a sua lista para ser usado como base.')
                voltando_ao_menu()


    except FileNotFoundError:
        
        print("\nNão é possível sugerir mídias, pois você ainda não possui uma lista de desejos, de volta ao menu, vá em 'adicionar filme a lista de desejos' e crie uma!")
        voltando_ao_menu()        
