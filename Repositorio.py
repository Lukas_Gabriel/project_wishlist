import scripts

flag_repositorio = True

while flag_repositorio:    
    
    scripts.menu_repositorio()

    escolha = str(input('\nDigite o número da escolha que você deseja: '))


    if escolha == '1':

        scripts.adicionar_filme_no_repositorio()
    
    elif escolha == '2':

        scripts.deletar_filme_no_repositorio()

    elif escolha == '3':
        
        flag_repositorio = False
        print('\nAté mais, meu caro.\n')
    
    else:
        print('\nEscolha inválida, tente novamente..')   
        scripts.voltando_ao_menu()     
